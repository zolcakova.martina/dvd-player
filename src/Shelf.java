import java.util.ArrayList;

public class Shelf {

    private static ArrayList<Disc> discs = new ArrayList<Disc>();

    public static void showMenu() {
        System.out.println("Write a number of movie to play: ");

        for (int i = 0; i < discs.size(); i++) {
            Disc currentDisc = discs.get(i);
            System.out.println(i + " - " + currentDisc.getTitle());
        }

        System.out.println("Any other number to stop watching.");
    }

    public static void addDisc(Disc disc) {
        discs.add(disc);
    }

    public static Disc getDisc(int index) {
        return discs.get(index);
    }

    public static int getDiscsSize() {
        return discs.size();
    }


}
