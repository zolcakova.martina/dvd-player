import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Disc titanicDisc = new Disc();
        titanicDisc.setTitle("Titanic");
        titanicDisc.setGenre("Romance/Drama");
        titanicDisc.setDuration(3, 14, 0);
        titanicDisc.setAboutTheMovie("Seventeen-year-old Rose hails from an aristocratic family and is set to be married. " +
                "When she boards the Titanic, she meets Jack Dawson, an artist, and falls in love with him.");

        Disc theShawshankRedemptionDisc = new Disc();
        theShawshankRedemptionDisc.setTitle("The Shawshank Redemption");
        theShawshankRedemptionDisc.setGenre("Drama/Crime");
        theShawshankRedemptionDisc.setDuration(2, 22, 0);
        theShawshankRedemptionDisc.setAboutTheMovie("Andy Dufresne, a successful banker, is arrested for the murders of his wife and her lover, " +
                "and is sentenced to life imprisonment at the Shawshank prison. " +
                "He becomes the most unconventional prisoner.");

        Disc forrestGumpDisc = new Disc();
        forrestGumpDisc.setTitle("Forrest Gump");
        forrestGumpDisc.setGenre("Drama/Romance");
        forrestGumpDisc.setDuration(2, 30, 0);
        forrestGumpDisc.setAboutTheMovie("Forrest, a man with low IQ, recounts the early years of his life when he found himself in " +
                "the middle of key historical events. All he wants now is to be reunited with his childhood sweetheart, Jenny.");

        Shelf.addDisc(titanicDisc);
        Shelf.addDisc(theShawshankRedemptionDisc);
        Shelf.addDisc(forrestGumpDisc);

        DVDPlayer.powerOn();

        while (true) {
            Shelf.showMenu();
            Scanner scanner = new Scanner(System.in);
            int choice = scanner.nextInt();
            if (choice >= 0 && choice < Shelf.getDiscsSize()) {
                DVDPlayer.insertDisc(Shelf.getDisc(choice));
                DVDPlayer.playCurrentDisc();
            } else {
                DVDPlayer.powerOff();
                break;
            }
        }

    }

}

