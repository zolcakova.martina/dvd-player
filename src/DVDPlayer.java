public class DVDPlayer {

    private static Disc currentDisc;

    public static void powerOn() {
        System.out.println("Welcome to your DVDPlayer!");
    }

    public static void powerOff() {
        System.out.println("Shutting down DVDPlayer.");
    }

    public static void insertDisc(Disc disc){
        currentDisc = disc;
    }

    public static void playCurrentDisc(){
        System.out.println(currentDisc.getTitle());
        System.out.println(currentDisc.getGenre());
        System.out.println(currentDisc.getDuration());
        System.out.println(currentDisc.getAboutTheMovie());
        System.out.println();
    }

}
