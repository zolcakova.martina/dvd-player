public class Disc {

    private String title;
    private String genre;
    private String aboutTheMovie;
    private String duration;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAboutTheMovie() {
        return aboutTheMovie;
    }

    public void setAboutTheMovie(String aboutTheMovie) {
        this.aboutTheMovie = aboutTheMovie;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(int hours, int minutes, int seconds){
        duration = hours + "h " + minutes + "min " + seconds + "sec";
    }

}
